<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="pr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<pr:isesesev3Main>

<c:choose>
	<c:when test="${not empty addedIncident.location}">
	<p>
	Intsident toimus:
	<label>${addedIncident.startDate}</label>
	</p>
	<p>
	Intsident märgiti lõppenuks:
	<label>${addedIncident.startDate}</label>
	</p>
	<p>
	Intsidendi toimumiskoht:
	<label>${addedIncident.location}</label>
	</p>
	<p>	
	Intsidendi lühikirjeldus:
	<label>${addedIncident.description}</label>	
	</p>
	<p>	
	Intsidendis osavõtnute valvurite arv:
	<label>${addedIncident.involvedGuardCount}</label>	
	</p>	
	</c:when>
	<c:otherwise>
	<h2>
		Intsidendi raport:
	</h2>
		<form method="POST">
			<p>
			<label>Millal toimus?</label><br />
			<input type="datetime" name="startDate" required/> <br />
			</p>
			<p>
			<label>Millal lõppeb?</label><br />
			<input type="datetime" name="age" required /> <br />
			</p>
			<p>
			<label>Kus toimus?</label><br />
			<input type="text" name="location" required /> <br />
			</p>
			<p>
			<label>Lühikirjeldus</label><br />
			<textarea name="description" rows="4" cols="4"></textarea><br />
			</p>
			<p>			
			<label>Mitu valvurit võttis osa?</label><br />
			<input type="number" name="involvedGuardCount" min="1" required /> <br />
			</p>
			<input type="submit" value="Lisa" />
		</form>
	</c:otherwise>
</c:choose>

</pr:isesesev3Main>