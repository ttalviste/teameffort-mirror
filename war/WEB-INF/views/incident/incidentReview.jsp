<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="pr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<pr:isesesev3Main>

<c:choose>
	<c:when test="${not empty addedIncident.location}">
	<label>${addedIncident.startDate}</label>
	<label>${addedIncident.location}</label>
	<label>${addedIncident.description}</label>	
	</c:when>
	<c:otherwise>
	<h2>
		Intsidendi raport:
	</h2>
		<form action="/Team4Great/incident/addNewIncident" method="POST">
			<p>
			<label>Millal toimus?</label><br />
			<input type="datetime" name="startDate" required/> <br />
			</p>
			<p>
			<label>Millal lõppeb?</label><br />
			<input type="datetime" name="age" required /> <br />
			</p>
			<p>
			<label>Kus toimus?</label><br />
			<input type="text" name="location" required /> <br />
			</p>
			<p>
			<label>Lühikirjeldus</label><br />
			<textarea name="description" rows="4" cols="4"></textarea><br />
			</p>
			<p>			
			<label>Mitu valvurit võttis osa?</label><br />
			<input type="number" name="involvedGuardCount" min="1" required /> <br />
			</p>
			<input type="submit" value="Lisa" />
		</form>
	</c:otherwise>
</c:choose>

</pr:isesesev3Main>